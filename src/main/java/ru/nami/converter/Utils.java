package ru.nami.converter;

import java.io.File;
import java.io.IOException;

/**
 * Created by Dmitriy Dzhevaga on 14.10.2015.
 */
public class Utils {
    private Utils() {
    }

    public static String getExtension(File file) {
        String absolutePath = file.getAbsolutePath();
        return absolutePath.substring(absolutePath.lastIndexOf(".") + 1);
    }

    public static File changeExtension(File file, String extension) {
        return new File(file.getAbsolutePath().replaceAll("[^.]+$", extension));
    }

    public static File getCurrentPath() {
        return new File(".");
    }

    public static void openInExplorer(File file) {
        try {
            Runtime.getRuntime().exec(new String[]{"explorer.exe", "/select,", "\"" + file.getAbsolutePath() + "\""});
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getVersion(Object object) {
        return object.getClass().getPackage().getImplementationVersion();
    }
}
