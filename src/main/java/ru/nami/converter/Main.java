package ru.nami.converter;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.*;

/**
 * Created by Dmitriy Dzhevaga on 13.10.2015.
 */
public class Main extends Application {
    private static final String LOG_FILE_NAME = "error.log";
    private static final String FXML_PATH = "/view.fxml";

    private static final Logger logger = Logger.getLogger(Main.class.getPackage().getName());

    static {
        try {
            Handler handler = new FileHandler(LOG_FILE_NAME);
            Formatter formatter = new SimpleFormatter();
            handler.setFormatter(formatter);
            logger.addHandler(handler);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(final String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(FXML_PATH));
        Parent view = loader.load();
        primaryStage.setScene(new Scene(view));
        Controller controller = loader.getController();
        controller.initializeStage(primaryStage);
    }
}