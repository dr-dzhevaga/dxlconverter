package ru.nami.converter.reader;

import ru.nami.converter.table.DoubleTable;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Dmitriy Dzhevaga on 23.10.2015.
 */
public class DxlFileReader implements FileReader {
    private static final String SEPARATOR = "\t";

    private DxlFileReader() {
    }

    public static DxlFileReader newInstance() {
        return new DxlFileReader();
    }

    private static List<String> readHeaders(BufferedReader reader) throws IOException {
        return Arrays.stream(reader.readLine().split(SEPARATOR, -1)).
                map(DxlFileReader::makeHeaderName).
                collect(Collectors.toList());
    }

    private static List<double[]> readRows(BufferedReader reader) throws IOException {
        return reader.lines().
                map(line -> line.split(SEPARATOR, -1)).
                map(row -> Arrays.stream(row).mapToDouble(DxlFileReader::convertToDouble).toArray()).
                collect(Collectors.toList());
    }

    private static List<double[]> convertToColumns(List<double[]> rows) throws IOException {
        List<double[]> columns = new ArrayList<>();
        int width = rows.get(0).length;
        int height = rows.size();
        IntStream.range(0, width).forEach(columnIndex -> {
            double[] column = IntStream.range(0, height).
                    mapToDouble(rowIndex -> rows.get(rowIndex)[columnIndex]).
                    toArray();
            columns.add(column);
        });
        return columns;
    }

    private static String makeHeaderName(String columnName) {
        int index = columnName.indexOf("\\");
        if (index > -1) {
            return columnName.substring(0, index);
        } else {
            return columnName;
        }
    }

    private static double convertToDouble(String string) {
        if (string == null || string.isEmpty()) {
            return Double.NaN;
        } else {
            try {
                return Double.valueOf(string.replace(",", "."));
            } catch (NumberFormatException e) {
                return Double.NaN;
            }
        }
    }

    @Override
    public DoubleTable read(File file) throws IOException {
        try (BufferedReader reader = Files.newBufferedReader(file.toPath())) {
            List<String> headers = readHeaders(reader);
            List<double[]> rows = readRows(reader);
            List<double[]> columns = convertToColumns(rows);
            return DoubleTable.newInstance(columns, headers);
        }
    }
}
