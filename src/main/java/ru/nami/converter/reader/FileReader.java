package ru.nami.converter.reader;

import ru.nami.converter.table.DoubleTable;

import java.io.File;
import java.io.IOException;

/**
 * Created by Dmitriy Dzhevaga on 23.10.2015.
 */
public interface FileReader {
    DoubleTable read(File file) throws IOException;
}
