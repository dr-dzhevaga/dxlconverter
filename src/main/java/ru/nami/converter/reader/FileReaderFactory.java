package ru.nami.converter.reader;

/**
 * Created by Dmitriy Dzhevaga on 23.10.2015.
 */
public class FileReaderFactory {
    private static final String UNKNOWN_FILE_FORMAT = "Unknown file format";

    public static FileReader newReader(InFileFormat inFileFormat) {
        switch (inFileFormat) {
            case DXL:
                return DxlFileReader.newInstance();
            case MDF:
            case DAT:
                return MdfFileReader.newInstance();
        }
        throw new IllegalArgumentException(UNKNOWN_FILE_FORMAT);
    }
}
