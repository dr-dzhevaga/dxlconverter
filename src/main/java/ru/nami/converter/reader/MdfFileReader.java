package ru.nami.converter.reader;

import ru.nami.converter.table.DoubleTable;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.DoubleStream;

/**
 * Created by Dmitriy Dzhevaga on 26.10.2015.
 */
public class MdfFileReader implements FileReader {

    private static final int HDBLOCK_START = 64;
    private static final int DGBLOCK_LINK_OFFSET = 4;
    private static final int NEXT_BLOCK_LINK_OFFSET = 4;
    private static final int CGBLOCK_LINK_OFFSET = 8;
    private static final int DBLOCK_LINK_OFFSET = 16;
    private static final int NUMBER_OF_CHANNEL_IDS_OFFSET = 22;
    private static final int CNBLOCK_LINK_OFFSET = 8;
    private static final int CCBLOCK_LINK_OFFSET = 8;
    private static final int SIZE_OD_DATA_RECORD_OFFSET = 20;
    private static final int NUMBER_OF_RECORDS_OFFSET = 22;
    private static final int SIGNAL_NAME_OFFSET = 26;
    private static final int SIGNAL_NAME_LENGTH = 32;
    private static final int START_OFFSET_IN_BITS_OFFSET = 186;
    private static final int NUMBER_OF_BITS_OFFSET = 188;
    private static final int SIGNAL_DATA_TYPE_OFFSET = 190;
    private static final int CONVERSION_TYPE_OFFSET = 42;
    private static final int SIZE_INFORMATION_OFFSET = 44;
    private static final int CONVERSION_DATA_OFFSET = 46;

    private static final String UNEXPECTED_BLOCK_ERROR = "%s block is expected at address 0x%s";
    private static final String UNEXPECTED_END_OF_FILE_ERROR = "Unexpected end of file";
    private static final String NOT_IMPLEMENTED_DATA_TYPE_ERROR = "Signal data type %s with %s length is not implemented";
    private static final String NOT_IMPLEMENTED_CONVERSION_TYPE_ERROR = "Conversion type %s with %s parameters size is not implemented";

    public MdfFileReader() {
    }

    public static MdfFileReader newInstance() {
        return new MdfFileReader();
    }

    private static DoubleTable convertToTable(List<Map<String, double[]>> signals) {
        List<String> headers = new ArrayList<>();
        signals.stream().map(Map::keySet).forEach(headers::addAll);

        Set<Double> globalTimeSet = new HashSet<>(100000);
        signals.forEach(channelSignals -> {
            double[] timeValues = channelSignals.entrySet().stream().
                    filter(entry -> entry.getKey().startsWith("time")).
                    map(Map.Entry::getValue).
                    findFirst().get();
            DoubleStream.of(timeValues).forEach(globalTimeSet::add);
        });
        double[] globalTime = globalTimeSet.stream().mapToDouble(Double::doubleValue).sorted().toArray();

        List<double[]> globalTimeScaledSignals = new ArrayList<>();
        signals.forEach(channelSignals -> {
            double[] signalTime = channelSignals.entrySet().stream().
                    filter(entry -> entry.getKey().startsWith("time")).
                    map(Map.Entry::getValue).
                    findFirst().get();
            channelSignals.values().stream().forEach(signal -> {
                double[] scaledSignal = Arrays.stream(globalTime).map(time -> {
                    int index = Arrays.binarySearch(signalTime, time);
                    if (index >= 0 && index < signal.length) {
                        return signal[index];
                    } else {
                        return Double.NaN;
                    }
                }).toArray();
                globalTimeScaledSignals.add(scaledSignal);
            });
        });

        headers.add(0, "time");
        globalTimeScaledSignals.add(0, globalTime);

        return DoubleTable.newInstance(globalTimeScaledSignals, headers);
    }

    private static List<Map<String, double[]>> readSignals(SeekableByteChannel byteChannel) throws IOException {
        List<Map<String, double[]>> signals = new ArrayList<>();
        checkBlockTypeId(byteChannel, HDBLOCK_START, BlockType.HDBLOCK);
        long dataGroupBlockLink = readLINK(byteChannel, HDBLOCK_START + DGBLOCK_LINK_OFFSET);
        checkBlockTypeId(byteChannel, dataGroupBlockLink, BlockType.DGBLOCK);
        while (dataGroupBlockLink != 0) {
            long nextDataGroupBlockLink = readLINK(byteChannel, dataGroupBlockLink + NEXT_BLOCK_LINK_OFFSET);
            long channelGroupBlockLink = readLINK(byteChannel, dataGroupBlockLink + CGBLOCK_LINK_OFFSET);
            long dataBlockLink = readLINK(byteChannel, dataGroupBlockLink + DBLOCK_LINK_OFFSET);
            int numberOfRecordIDs = readUINT16(byteChannel, dataGroupBlockLink + NUMBER_OF_CHANNEL_IDS_OFFSET);
            while (channelGroupBlockLink > 0) {
                checkBlockTypeId(byteChannel, channelGroupBlockLink, BlockType.CGBLOCK);
                long nextChannelGroupBlockLink = readLINK(byteChannel, channelGroupBlockLink + NEXT_BLOCK_LINK_OFFSET);
                long channelBlockLink = readLINK(byteChannel, channelGroupBlockLink + CNBLOCK_LINK_OFFSET);
                int sizeOfRecord = readUINT16(byteChannel, channelGroupBlockLink + SIZE_OD_DATA_RECORD_OFFSET);
                long numberOfRecords = readUINT32(byteChannel, channelGroupBlockLink + NUMBER_OF_RECORDS_OFFSET);
                RawData rawData = RawData.readFromChannel(byteChannel, dataBlockLink, sizeOfRecord, (int) numberOfRecords, numberOfRecordIDs);
                Map<String, double[]> channelSignals = new LinkedHashMap<>((int) numberOfRecords);
                while (channelBlockLink != 0) {
                    checkBlockTypeId(byteChannel, channelBlockLink, BlockType.CNBLOCK);
                    long channelConversionBlock = readLINK(byteChannel, channelBlockLink + CCBLOCK_LINK_OFFSET);
                    long nextChannelBlockLink = readLINK(byteChannel, channelBlockLink + NEXT_BLOCK_LINK_OFFSET);
                    String signalName = readCHAR(byteChannel, channelBlockLink + SIGNAL_NAME_OFFSET, SIGNAL_NAME_LENGTH);
                    String headerName = makeHeaderName(signalName);
                    int startOffset = readUINT16(byteChannel, channelBlockLink + START_OFFSET_IN_BITS_OFFSET);
                    int numberOfBits = readUINT16(byteChannel, channelBlockLink + NUMBER_OF_BITS_OFFSET);
                    int signalDataType = readUINT16(byteChannel, channelBlockLink + SIGNAL_DATA_TYPE_OFFSET);
                    checkBlockTypeId(byteChannel, channelConversionBlock, BlockType.CCBLOCK);
                    int conversionType = readUINT16(byteChannel, channelConversionBlock + CONVERSION_TYPE_OFFSET);
                    int sizeInformation = readUINT16(byteChannel, channelConversionBlock + SIZE_INFORMATION_OFFSET);
                    double[] conversionData = readREAL(byteChannel, channelConversionBlock + CONVERSION_DATA_OFFSET, sizeInformation);
                    double[] data = rawData.asDoubleArray(startOffset, numberOfBits, signalDataType, conversionType, conversionData);
                    channelSignals.put(headerName, data);
                    channelBlockLink = nextChannelBlockLink;
                }
                signals.add(channelSignals);
                channelGroupBlockLink = nextChannelGroupBlockLink;
            }
            dataGroupBlockLink = nextDataGroupBlockLink;
        }
        return signals;
    }

    private static String readCHAR(SeekableByteChannel channel, long position, int length) throws IOException {
        channel.position(position);
        ByteBuffer buffer = ByteBuffer.allocate(length);
        int read = channel.read(buffer);
        checkResultLength(length, read);
        return new String(buffer.array());
    }

    private static int readUINT16(SeekableByteChannel channel, long position) throws IOException {
        channel.position(position);
        ByteBuffer byteBuffer = ByteBuffer.allocate(2);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        int read = channel.read(byteBuffer);
        checkResultLength(2, read);
        return Short.toUnsignedInt(byteBuffer.getShort(0));
    }

    private static long readUINT32(SeekableByteChannel channel, long position) throws IOException {
        channel.position(position);
        ByteBuffer byteBuffer = ByteBuffer.allocate(4);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        int read = channel.read(byteBuffer);
        checkResultLength(4, read);
        return Integer.toUnsignedLong(byteBuffer.getInt(0));
    }

    private static double readREAL(SeekableByteChannel channel, long position) throws IOException {
        channel.position(position);
        ByteBuffer byteBuffer = ByteBuffer.allocate(8);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        int read = channel.read(byteBuffer);
        checkResultLength(8, read);
        return byteBuffer.getDouble(0);
    }

    private static double[] readREAL(SeekableByteChannel channel, long startPosition, int size) throws IOException {
        double[] result = new double[size];
        for (int i = 0; i < size; i++) {
            result[i] = readREAL(channel, startPosition + i * 8);
        }
        return result;
    }

    private static long readLINK(SeekableByteChannel channel, long position) throws IOException {
        return readUINT32(channel, position);
    }

    private static ByteBuffer readRawData(SeekableByteChannel channel, long position, int size) throws IOException {
        ByteBuffer byteBuffer = ByteBuffer.allocate(size);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        channel.position(position);
        int read = channel.read(byteBuffer);
        checkResultLength(size, read);
        return byteBuffer;
    }

    private static void checkBlockTypeId(SeekableByteChannel channel, long position, BlockType blockType) throws IOException {
        String id = readCHAR(channel, position, 2);
        if (!id.equals(blockType.getId())) {
            throw new IllegalStateException(String.format(UNEXPECTED_BLOCK_ERROR,
                    blockType.name(), Long.toHexString(position)));
        }
    }

    private static void checkResultLength(int expected, int actual) {
        if (expected != actual) {
            throw new IllegalStateException(UNEXPECTED_END_OF_FILE_ERROR);
        }
    }

    private static String makeHeaderName(String columnName) {
        String name = columnName.trim();
        int index = name.indexOf("\\");
        if (index > -1) {
            return name.substring(0, index);
        } else {
            return name;
        }
    }

    @Override
    public DoubleTable read(File file) throws IOException {
        try (SeekableByteChannel byteChannel = Files.newByteChannel(file.toPath())) {
            List<Map<String, double[]>> signals = readSignals(byteChannel);
            return convertToTable(signals);
        }
    }

    private enum BlockType {
        HDBLOCK("HD"),
        DGBLOCK("DG"),
        CGBLOCK("CG"),
        CNBLOCK("CN"),
        CCBLOCK("CC");

        private final String id;

        BlockType(String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }
    }

    private enum SignalDataType {
        UINT_1,
        UINT_8,
        UINT_16,
        UINT_32,
        INT_8,
        INT_16,
        INT_32,
        FLOAT,
        DOUBLE,
        STRING;

        public static SignalDataType getDataType(int dataType, int numberOfBits) {
            switch (dataType) {
                case 0:
                    switch (numberOfBits) {
                        case 1:
                            return UINT_1;
                        case 8:
                            return UINT_8;
                        case 16:
                            return UINT_16;
                        case 32:
                            return UINT_32;
                        default:
                            break;
                    }
                case 1:
                    switch (numberOfBits) {
                        case 8:
                            return INT_8;
                        case 16:
                            return INT_16;
                        case 32:
                            return INT_32;
                        default:
                            break;
                    }
                case 2:
                    if (numberOfBits >= 64) {
                        return DOUBLE;
                    } else if (numberOfBits >= 32) {
                        return FLOAT;
                    }
                case 3:
                    if (numberOfBits >= 64) {
                        return DOUBLE;
                    }
                case 7:
                    return STRING;
                default:
                    break;
            }
            throw new IllegalArgumentException(String.format(NOT_IMPLEMENTED_DATA_TYPE_ERROR,
                    dataType, numberOfBits));
        }
    }

    private enum ConversionType {
        PARAMETRIC_LINEAR_2,
        COMPU_VTAB,
        NO_CONVERSION;

        public static ConversionType getConversionType(int conversionType, int sizeInformation) {
            switch (conversionType) {
                case 0:
                    if (sizeInformation == 2) {
                        return PARAMETRIC_LINEAR_2;
                    } else {
                        break;
                    }
                case 11:
                    return COMPU_VTAB;
                case 0xFFFF:
                    return NO_CONVERSION;
            }
            throw new IllegalArgumentException(String.format(NOT_IMPLEMENTED_CONVERSION_TYPE_ERROR,
                    conversionType, sizeInformation));
        }
    }

    private static class RawData {
        private final ByteBuffer rawData;
        private final int sizeOfRecord;
        private final int numberOfRecords;
        private final int numberOfRecordIDs;

        private RawData(SeekableByteChannel byteChannel, long position, int sizeOfRecord, int numberOfRecords, int numberOfRecordIDs) throws IOException {
            int rowDataSize = (sizeOfRecord + numberOfRecordIDs) * numberOfRecords;
            this.rawData = readRawData(byteChannel, position, rowDataSize);
            this.sizeOfRecord = sizeOfRecord;
            this.numberOfRecords = numberOfRecords;
            this.numberOfRecordIDs = numberOfRecordIDs;
        }

        public static RawData readFromChannel(SeekableByteChannel byteChannel, long position, int sizeOfRecord, int numberOfRecords, int numberOfRecordIDs) throws IOException {
            return new RawData(byteChannel, position, sizeOfRecord, numberOfRecords, numberOfRecordIDs);
        }

        private static double convert(double data, int conversionType, double[] conversionData) {
            ConversionType type = ConversionType.getConversionType(conversionType, conversionData.length);
            switch (type) {
                case PARAMETRIC_LINEAR_2:
                    return data * conversionData[1] + conversionData[0];
                default:
                    return data;
            }
        }

        public double[] asDoubleArray(int startOffset, int numberOfBits, int signalDataType, int conversionType, double[] conversionData) {
            SignalDataType dataType = SignalDataType.getDataType(signalDataType, numberOfBits);
            if (dataType == SignalDataType.STRING) {
                return new double[0];
            }
            double[] result = new double[numberOfRecords];
            int startOffsetInBytes = startOffset / 8;
            int bitOffset = startOffset % 8;
            int recordStart = 0;
            for (int i = 0; i < numberOfRecords; i++) {
                int signalStart = recordStart + startOffsetInBytes;
                recordStart += sizeOfRecord;
                if (numberOfRecordIDs > 0) {
                    signalStart += 1;
                }
                switch (dataType) {
                    case UINT_1:
                        result[i] = (rawData.get(signalStart) >> bitOffset) & 0x01;
                        break;
                    case UINT_8:
                        result[i] = Byte.toUnsignedInt(rawData.get(signalStart));
                        break;
                    case UINT_16:
                        result[i] = Short.toUnsignedInt(rawData.getShort(signalStart));
                        break;
                    case UINT_32:
                        result[i] = Integer.toUnsignedLong(rawData.getInt(signalStart));
                        break;
                    case INT_8:
                        result[i] = rawData.get(signalStart);
                        break;
                    case INT_16:
                        result[i] = rawData.getShort(signalStart);
                        break;
                    case INT_32:
                        result[i] = rawData.getInt(signalStart);
                        break;
                    case FLOAT:
                        result[i] = rawData.getFloat(signalStart);
                        break;
                    case DOUBLE:
                        result[i] = rawData.getDouble(signalStart);
                        break;
                }
                result[i] = convert(result[i], conversionType, conversionData);
            }
            return result;
        }
    }
}
