package ru.nami.converter.reader;

/**
 * Created by Dmitriy Dzhevaga on 27.10.2015.
 */
public enum InFileFormat {
    MDF,
    DAT,
    DXL;

    public static InFileFormat of(String name) {
        for (InFileFormat value : InFileFormat.class.getEnumConstants()) {
            if (value.name().compareToIgnoreCase(name) == 0) {
                return value;
            }
        }
        throw new IllegalArgumentException("Unknown file format: " + name);
    }
}
