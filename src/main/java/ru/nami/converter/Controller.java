package ru.nami.converter;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Cursor;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import ru.nami.converter.reader.FileReader;
import ru.nami.converter.reader.FileReaderFactory;
import ru.nami.converter.reader.InFileFormat;
import ru.nami.converter.table.Column;
import ru.nami.converter.table.DoubleTable;
import ru.nami.converter.writer.FileWriter;
import ru.nami.converter.writer.FileWriterFactory;
import ru.nami.converter.writer.OutFileFormat;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Dmitriy Dzhevaga on 15.10.2015.
 */
public class Controller implements Initializable {
    private static final String PROGRAM_TITLE = "Converter v.%s";
    private static final String ICON_PATH = "/icon.png";
    private static final String MDF_EXTENSION = "*.mdf";
    private static final String DAT_EXTENSION = "*.dat";
    private static final String MDF_DESCRIPTION = "ETAS mdf";
    private static final String DXL_EXTENSION = "*.dxl";
    private static final String DXL_DESCRIPTION = "ETAS dxl";
    private static final String TIME_COLUMN_NAME = "time";
    private static final String EXCLUDE_SIGNAL_NAME_PREFIX = "$";
    private static final String TIME_AXIS_DOUBLE_PATTERN = "#0";
    private static final String NO_FILE = "";
    private static final String MERGED_FILES = "Merged files";
    private static final String[] NOTHING_TO_CONVERT = {"Nothing to convert.", "Please select a time axis and at least one signal."};
    private static final String[] CONVERTING_ERROR = {"Error while converting.", "See log for details."};
    private static final String[] READING_ERROR = {"Error while reading.", "See log for details."};

    private static final Logger logger = Logger.getLogger(Controller.class.getName());

    @FXML
    private Label inFileLabel;
    @FXML
    private ChoiceBox<Column> timeAxisChoiceBox;
    @FXML
    private ListView<Column> signalListView;
    @FXML
    private ChoiceBox<Integer> decimationChoiceBox;
    @FXML
    private ChoiceBox<OutFileFormat> outFormatChoiceBox;

    private Stage stage;
    private FileChooser fileChooser = new FileChooser();
    private File inFile;
    private DoubleTable table = DoubleTable.newInstance();
    private List<Column> selectedSignals = new ArrayList<>();
    private Column selectedTimeAxis;

    @Override
    public void initialize(URL fxmlFileLocation, ResourceBundle resources) {
        signalListView.getSelectionModel().selectionModeProperty().setValue(SelectionMode.MULTIPLE);
        outFormatChoiceBox.getItems().addAll(OutFileFormat.values());
        outFormatChoiceBox.getSelectionModel().selectFirst();
        fileChooser.setInitialDirectory(Utils.getCurrentPath());
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(MDF_DESCRIPTION, MDF_EXTENSION, DAT_EXTENSION));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(DXL_DESCRIPTION, DXL_EXTENSION));
    }

    public void initializeStage(Stage primaryStage) {
        stage = primaryStage;
        stage.getIcons().add(new Image(getClass().getResourceAsStream(ICON_PATH)));
        stage.setTitle(String.format(PROGRAM_TITLE, Utils.getVersion(this)));
        stage.show();
        onOpen();
    }

    @FXML
    private void onOpen() {
        inFileLabel.setText(NO_FILE);
        signalListView.getItems().clear();
        timeAxisChoiceBox.getItems().clear();
        processFile(resultTable -> {
            table = resultTable;
            List<String> headers = table.getHeaders();
            signalListView.getItems().addAll(
                    IntStream.range(0, headers.size()).
                            filter(i -> !headers.get(i).isEmpty()).
                            filter(i -> !headers.get(i).startsWith(EXCLUDE_SIGNAL_NAME_PREFIX)).
                            filter(i -> !headers.get(i).equals(TIME_COLUMN_NAME)).
                            mapToObj(i -> new Column(headers.get(i), i)).
                            collect(Collectors.toList())
            );
            selectedSignals.forEach(column -> signalListView.getSelectionModel().select(column));
            timeAxisChoiceBox.getItems().addAll(
                    IntStream.range(0, headers.size()).
                            filter(i -> headers.get(i).equals(TIME_COLUMN_NAME)).
                            mapToObj(i -> new Column(makeTimeAxisName(table, i), i)).
                            collect(Collectors.toList())
            );
            timeAxisChoiceBox.getSelectionModel().select(selectedTimeAxis);
            inFileLabel.setText(inFile.getPath());
        });
    }

    @FXML
    public void onAdd(ActionEvent actionEvent) {
        if(table.getSize() == 0) {
            onOpen();
        } else {
            processFile(resultTable -> {
                table.merge(resultTable);
                inFileLabel.setText(MERGED_FILES);
            });
        }
    }

    @FXML
    private void onProcess() {
        boolean timeAxisIsNotSelected = timeAxisChoiceBox.getSelectionModel().isEmpty();
        boolean signalIsNotSelected = signalListView.getSelectionModel().isEmpty();
        if (timeAxisIsNotSelected || signalIsNotSelected) {
            showWarning(NOTHING_TO_CONVERT);
            return;
        }
        selectedTimeAxis = timeAxisChoiceBox.getSelectionModel().getSelectedItem();
        selectedSignals = new ArrayList<>(signalListView.getSelectionModel().getSelectedItems());
        int decimationStep = decimationChoiceBox.getSelectionModel().getSelectedItem();
        FileWriter fileWriter = FileWriterFactory.newWriter(outFormatChoiceBox.getValue());
        File outFile = Utils.changeExtension(inFile, fileWriter.getOutFileExtension());
            stage.getScene().setCursor(Cursor.WAIT);
        CompletableFuture.runAsync(() -> {
            try {
                int timeAxisIndex = selectedTimeAxis.getIndex();
                List<Integer> signalIndexes = selectedSignals.stream().map(Column::getIndex).collect(Collectors.toList());
                DoubleTable tableCopy = DoubleTable.copy(table);
                tableCopy.extrapolate(signalIndexes);
                tableCopy.filter(timeAxisIndex);
                tableCopy.decimate(decimationStep);
                List<Integer> outputIndexes = new ArrayList<>(signalIndexes);
                outputIndexes.add(0, timeAxisIndex);
                fileWriter.write(outFile, tableCopy, outputIndexes);
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }).whenComplete((result, exception) -> Platform.runLater(() -> {
            stage.getScene().setCursor(Cursor.DEFAULT);
            if (exception != null) {
                handleException(CONVERTING_ERROR, exception);
            } else {
                Utils.openInExplorer(outFile);
            }
        }));
    }

    @FXML
    public void onSort(ActionEvent actionEvent) {
        signalListView.getItems().sort((s1, s2) -> s1.getName().compareToIgnoreCase(s2.getName()));
    }

    private void processFile(Consumer<DoubleTable> tableProcessor) {
        inFile = fileChooser.showOpenDialog(stage);
        if (inFile == null) {
            return;
        }
        fileChooser.setInitialDirectory(inFile.getParentFile());
        stage.getScene().setCursor(Cursor.WAIT);
        CompletableFuture.supplyAsync(() -> {
            try {
                String inFileExtension = Utils.getExtension(inFile);
                FileReader fileReader = FileReaderFactory.newReader(InFileFormat.of(inFileExtension));
                return fileReader.read(inFile);
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }).whenComplete((resultTable, exception) -> Platform.runLater(() -> {
            if (exception != null) {
                handleException(READING_ERROR, exception);
            } else if (resultTable != null) {
                tableProcessor.accept(resultTable);
            }
            stage.getScene().setCursor(Cursor.DEFAULT);
        }));
    }

    private String makeTimeAxisName(DoubleTable table, int columnIndex) {
        double stepLength = table.getStepLength(columnIndex);
        return new DecimalFormat(TIME_AXIS_DOUBLE_PATTERN).format(Math.round(stepLength * 1000));
    }

    private void handleException(String message[], Throwable exception) {
        logger.log(Level.SEVERE, exception.getMessage(), exception);
        showAlert(message, Alert.AlertType.ERROR);
    }

    private void showWarning(String message[]) {
        showAlert(message, Alert.AlertType.WARNING);
    }

    private void showAlert(String message[], Alert.AlertType alertType) {
        Alert alert = new Alert(alertType);
        alert.setHeaderText(message[0]);
        alert.setContentText(message[1]);
        alert.showAndWait();
    }
}
