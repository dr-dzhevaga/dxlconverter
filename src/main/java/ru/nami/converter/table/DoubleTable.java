package ru.nami.converter.table;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

import static java.lang.Double.isNaN;

/**
 * Created by Dmitriy Dzhevaga on 13.10.2015.
 */
public final class DoubleTable {
    private static final String WRONG_WIDTH_ERROR = "Data rows and headers must have the same width";
    private static final String WRONG_SIZE_ERROR = "All columns must have the same size";
    private static final String WRONG_MERGE_TABLE = "Merged tables must have the same width";

    private List<String> headers;
    private List<double[]> columns;

    private DoubleTable(List<double[]> columns, List<String> headers) {
        checkDataAndHeaders(columns, headers);
        this.columns = columns;
        this.headers = headers;
    }

    public static DoubleTable newInstance(List<double[]> data, List<String> headers) {
        return new DoubleTable(data, headers);
    }

    public static DoubleTable newInstance() {
        return new DoubleTable(new ArrayList<>(), new ArrayList<>());
    }

    public static DoubleTable copy(DoubleTable table) throws IOException {
        List<double[]> dataCopy = table.columns.parallelStream().collect(Collectors.toList());
        List<String> headersCopy = table.headers.stream().collect(Collectors.toList());
        return new DoubleTable(dataCopy, headersCopy);
    }

    private static void checkDataAndHeaders(List<double[]> data, List<String> headers) {
        if (data.size() != headers.size()) {
            throw new IllegalStateException(WRONG_WIDTH_ERROR);
        }
        data.forEach(column -> {
            if (column.length != data.get(0).length) {
                throw new IllegalStateException(WRONG_SIZE_ERROR);
            }
        });
    }

    public void merge(DoubleTable table) {
        if(this.getWidth() != table.getWidth()) {
            throw new IllegalArgumentException(WRONG_MERGE_TABLE);
        }
        IntStream.range(0, this.getWidth()).forEach(i -> setColumn(i, merge(this.getColumn(i), table.getColumn(i))));
    }

    public double[] getColumn(int column) {
        return columns.get(column);
    }

    private void setColumn(int columnIndex, double[] columnData) {
        columns.set(columnIndex, columnData);
    }

    public double get(int row, int column) {
        return columns.get(column)[row];
    }

    public List<String> getHeaders() {
        return headers;
    }

    public String getHeader(int column) {
        return headers.get(column);
    }

    public int getSize() {
        if(getWidth() == 0) {
            return 0;
        } else {
            return columns.get(0).length;
        }
    }

    public int getWidth() {
        return columns.size();
    }

    public double getStepLength(int columnIndex) {
        double[] values = DoubleStream.of(columns.get(columnIndex)).
                filter(d -> !isNaN(d)).
                limit(10).
                toArray();
        if (values.length < 2) {
            return Double.NaN;
        }
        return IntStream.range(0, values.length - 1).
                mapToObj(i -> new double[]{values[i], values[i + 1]}).
                mapToDouble(valuePair -> valuePair[1] - valuePair[0]).
                average().
                getAsDouble();
    }

    public void extrapolate(List<Integer> columnIndexes) {
        columnIndexes.forEach(columnIndex -> {
            double[] column = columns.get(columnIndex);
            double lastNotNaN = DoubleStream.of(column).
                    filter(d -> !Double.isNaN(d)).
                    findFirst().
                    orElse(Double.NaN);
            for (int i = 0; i < column.length; i++) {
                if (isNaN(column[i])) {
                    column[i] = lastNotNaN;
                } else {
                    lastNotNaN = column[i];
                }
            }
        });
    }

    public void filter(int filterIndex) {
        double[] filterColumn = columns.get(filterIndex);
        IntStream.range(0, columns.size()).forEach(columnIndex -> {
            double[] currentColumn = columns.get(columnIndex);
            double[] filteredColumn = IntStream.range(0, currentColumn.length).
                    filter(i -> !isNaN(filterColumn[i])).
                    mapToDouble(i -> currentColumn[i]).
                    toArray();
            columns.set(columnIndex, filteredColumn);
        });
    }

    public void decimate(int step) {
        if (step > 1) {
            IntStream.range(0, columns.size()).forEach(columnIndex -> {
                double[] currentColumn = columns.get(columnIndex);
                double[] decimatedColumn = IntStream.iterate(0, i -> i + step).
                        limit((currentColumn.length - 1) / step).
                        mapToDouble(i -> currentColumn[i]).
                        toArray();
                columns.set(columnIndex, decimatedColumn);
            });
        }
    }

    private static double[] merge(double[] first,double[] second) {
        double[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }
}
