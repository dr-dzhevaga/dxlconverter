package ru.nami.converter.table;

import java.util.Objects;

/**
 * Created by Dmitriy Dzhevaga on 14.10.2015.
 */
public class Column {
    private final String name;
    private final int index;

    public Column(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Column)) {
            return false;
        }
        final Column other = (Column) obj;
        return Objects.equals(this.index, other.index) && Objects.equals(this.name, other.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, index);
    }

    @Override
    public String toString() {
        return name;
    }
}