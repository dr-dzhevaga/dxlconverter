package ru.nami.converter.writer;

import ru.nami.converter.table.DoubleTable;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.LongStream;

/**
 * Created by Dmitriy Dzhevaga on 13.10.2015.
 */
public final class MFileWriter implements FileWriter {
    private static final String OUTPUT_FILE_EXTENSION = "m";
    private static final String INDEXES_NAME = "pCNT";
    private static final String OUTPUT_SEPARATOR = "; ";
    private static final String OPEN_SIGNAL = "%s = [";
    private static final String CLOSE_SIGNAL = "];\n";

    private MFileWriter() {
    }

    public static MFileWriter newInstance() {
        return new MFileWriter();
    }

    private static void writeSignals(PrintWriter writer, DoubleTable table, List<Integer> columns) {
        columns.stream().mapToInt(Integer::intValue).forEach(columnIndex -> {
                    String fieldName = table.getHeader(columnIndex);
                    writer.print(String.format(OPEN_SIGNAL, fieldName));
                    DoubleStream.of(table.getColumn(columnIndex)).
                            mapToObj(Double::toString).
                            map(MFileWriter::addSeparator).
                            forEach(writer::print);
                    writer.print(CLOSE_SIGNAL);
                    writer.println();
                }
        );
    }

    private static void writeIndexes(PrintWriter writer, long length) {
        writer.print(String.format(OPEN_SIGNAL, INDEXES_NAME));
        LongStream.range(0, length).
                mapToObj(Long::toString).
                map(MFileWriter::addSeparator).
                forEach(writer::print);
        writer.print(CLOSE_SIGNAL);
    }

    private static String addSeparator(String data) {
        return data + OUTPUT_SEPARATOR;
    }

    @Override
    public void write(File file, DoubleTable table, List<Integer> columns) throws IOException {
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(file.toPath()))) {
            writeSignals(writer, table, columns);
            writeIndexes(writer, table.getSize());
        }
    }

    @Override
    public String getOutFileExtension() {
        return OUTPUT_FILE_EXTENSION;
    }
}
