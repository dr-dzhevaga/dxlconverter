package ru.nami.converter.writer;

import ru.nami.converter.table.DoubleTable;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.IntStream;

/**
 * Created by dmitriy.dzhevaga on 23.10.2015.
 */
public class CSVFileWriter implements FileWriter {
    private static final String OUTPUT_FILE_EXTENSION = "csv";
    private static final String OUTPUT_SEPARATOR = ";";

    private CSVFileWriter() {
    }

    public static CSVFileWriter newInstance() {
        return new CSVFileWriter();
    }

    private static void writeHeaders(PrintWriter writer, DoubleTable table, List<Integer> columns) {
        columns.stream().
                map(table::getHeader).
                map(CSVFileWriter::addSeparator).
                forEach(writer::print);
        writer.println();
    }

    private static void writeData(PrintWriter writer, DoubleTable table, List<Integer> columns) {
        IntStream.range(0, table.getSize()).forEach(row -> {
            columns.stream().
                    mapToDouble(column -> table.get(row, column)).
                    mapToObj(Double::toString).
                    map(CSVFileWriter::addSeparator).
                    forEach(writer::print);
            writer.println();
        });
    }

    private static String addSeparator(String data) {
        return data + OUTPUT_SEPARATOR;
    }

    @Override
    public void write(File file, DoubleTable table, List<Integer> columns) throws IOException {
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(file.toPath()))) {
            writeHeaders(writer, table, columns);
            writeData(writer, table, columns);
        }
    }

    @Override
    public String getOutFileExtension() {
        return OUTPUT_FILE_EXTENSION;
    }

}
