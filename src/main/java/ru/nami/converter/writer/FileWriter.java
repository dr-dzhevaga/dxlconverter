package ru.nami.converter.writer;

import ru.nami.converter.table.DoubleTable;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Dmitriy Dzhevaga on 23.10.2015.
 */
public interface FileWriter {
    void write(File file, DoubleTable table, List<Integer> columns) throws IOException;

    String getOutFileExtension();
}
