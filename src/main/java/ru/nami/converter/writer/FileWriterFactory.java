package ru.nami.converter.writer;

/**
 * Created by Dmitriy Dzhevaga on 23.10.2015.
 */
public class FileWriterFactory {
    private static final String UNKNOWN_FILE_FORMAT = "Unknown file format";

    public static FileWriter newWriter(OutFileFormat format) {
        switch (format) {
            case M:
                return MFileWriter.newInstance();
            case MAT:
                return MatFileWriter.newInstance();
            case CSV:
                return CSVFileWriter.newInstance();
            default:
                throw new IllegalArgumentException(UNKNOWN_FILE_FORMAT);
        }
    }
}
