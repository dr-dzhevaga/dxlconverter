package ru.nami.converter.writer;

/**
 * Created by Dmitriy Dzhevaga on 23.10.2015.
 */
public enum OutFileFormat {
    MAT,
    M,
    CSV;

    @Override
    public String toString() {
        return "." + name().toLowerCase();
    }
}