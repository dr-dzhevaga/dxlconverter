package ru.nami.converter.writer;

import com.jmatio.types.MLArray;
import com.jmatio.types.MLDouble;
import ru.nami.converter.table.DoubleTable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.DoubleStream;

/**
 * Created by Dmitriy Dzhevaga on 23.10.2015.
 */
public class MatFileWriter implements FileWriter {
    private static final String OUTPUT_FILE_EXTENSION = "mat";
    private static final String INDEXES_NAME = "pCNT";

    private final com.jmatio.io.MatFileWriter matFileWriter = new com.jmatio.io.MatFileWriter();

    private MatFileWriter() {
    }

    public static MatFileWriter newInstance() {
        return new MatFileWriter();
    }

    @Override
    public void write(File file, DoubleTable table, List<Integer> columns) throws IOException {
        List<MLArray> list = new ArrayList<>();
        prepareSignals(table, columns, list);
        prepareIndexes(table.getSize(), list);
        matFileWriter.write(file, list);
    }

    @Override
    public String getOutFileExtension() {
        return OUTPUT_FILE_EXTENSION;
    }

    private void prepareSignals(DoubleTable table, List<Integer> columns, List<MLArray> list) {
        columns.stream().mapToInt(Integer::intValue).forEach(columnIndex -> {
                    String fieldName = table.getHeader(columnIndex);
                    double[] data = table.getColumn(columnIndex);
                    MLDouble mlDouble = new MLDouble(fieldName, data, data.length);
                    list.add(mlDouble);
                }
        );
    }

    private void prepareIndexes(long length, List<MLArray> list) {
        double[] data = DoubleStream.iterate(0, d -> ++d).limit(length).toArray();
        MLDouble mlDouble = new MLDouble(INDEXES_NAME, data, data.length);
        list.add(mlDouble);
    }
}
